<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Aviso;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Aviso controller.
 *
 */
class AvisoController extends Controller {

    /**
     * Lists all unresolved aviso entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $avisos = $em->getRepository('AppBundle:Aviso')->findAllUnresolvedAvisos();
        return $this->render('aviso/index.html.twig', array(
                    'avisos' => $avisos,
        ));
    }

    /**
     * Lists all resolved aviso entities.
     *
     */
    public function indexResolvedAction() {
        $em = $this->getDoctrine()->getManager();
        $avisos = $em->getRepository('AppBundle:Aviso')->findAllResolvedAvisos();
        $retriever = $this->get('app.retrieve_last_data');
        $retriever->setDataToSession();
        return $this->render('aviso/index.html.twig', array(
                    'avisos' => $avisos,
        ));
    }

    /**
     * Lists all deleted aviso entities.
     *
     */
    public function indexDeletedAction() {
        $em = $this->getDoctrine()->getManager();
        $avisos = $em->getRepository('AppBundle:Aviso')->findAllDeletedAvisos();
        $retriever = $this->get('app.retrieve_last_data');
        $retriever->setDataToSession();

        return $this->render('aviso/index.html.twig', array(
                    'avisos' => $avisos,
        ));
    }

    /**
     * Creates a new aviso entity.
     *
     */
    public function newAction(Request $request) {
        $aviso = new Aviso();
        $form = $this->createForm('AppBundle\Form\AvisoType', $aviso);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($aviso);
            $em->flush($aviso);

            return $this->redirectToRoute('aviso_show', array('id' => $aviso->getId()));
        }

        return $this->render('aviso/new.html.twig', array(
                    'aviso' => $aviso,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a aviso entity.
     *
     */
    public function showAction(Aviso $aviso) {
        $deleteForm = $this->createDeleteForm($aviso);

        return $this->render('aviso/show.html.twig', array(
                    'aviso' => $aviso,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing aviso entity.
     *
     */
    public function editAction(Request $request, Aviso $aviso) {
        $deleteForm = $this->createDeleteForm($aviso);
        $editForm = $this->createForm('AppBundle\Form\AvisoType', $aviso);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('aviso_index');
        }

        return $this->render('aviso/edit.html.twig', array(
                    'aviso' => $aviso,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a aviso entity.
     *
     */
    public function deleteAction(Request $request, Aviso $aviso) {
        $form = $this->createDeleteForm($aviso);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($aviso);
            $em->flush($aviso);
        }

        return $this->redirectToRoute('aviso_index');
    }

    public function resolveAction(Request $request, Aviso $aviso) {

        $marcada = $aviso->getMarcada();

        if ($marcada) {
            $aviso->setMarcada(false);
        } else {
            $aviso->setMarcada(true);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($aviso);
        $em->flush();
        $retriever = $this->get('app.retrieve_last_data');
        $retriever->setDataToSession();

        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

    public function trashAction(Request $request, Aviso $aviso) {

        if ($aviso->getEliminado()) {
            $aviso->setEliminado(false);
        } else {
            $aviso->setEliminado(true);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($aviso);
        $em->flush();
        $retriever = $this->get('app.retrieve_last_data');
        $retriever->setDataToSession();
        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

    /**
     * Creates a form to delete a aviso entity.
     *
     * @param Aviso $aviso The aviso entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Aviso $aviso) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('aviso_delete', array('id' => $aviso->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    public function refreshAction(Request $request) {
        $retriever = $this->get('app.retrieve_last_data');
        /* $em = $this->getDoctrine()->getManager();
          $avisos = $em->getRepository('AppBundle:Aviso')->getLastAvisos(5);
          $tareas = $em->getRepository('AppBundle:Tarea')->getLastTareas(10);
          $session = $request->getSession();
          $session->set('Avisos', $avisos);
          $session->set('Tareas', $tareas); */
        $retriever->setDataToSession();
        $referer = $request->headers->get('referer');
        return $this->redirect($referer)
        ;
    }
    
    

}
