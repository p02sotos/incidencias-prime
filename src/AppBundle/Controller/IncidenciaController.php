<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Incidencia;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Form\IncidenciaType;

/**
 * Incidencia controller.
 *
 */
class IncidenciaController extends Controller {

    /**
     * Lists all Incidencia entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $incidencias = $em->getRepository('AppBundle:Incidencia')->findAllIncidencias();



        return $this->render('incidencia/index.html.twig', array(
                    'incidencias' => $incidencias,
        ));
    }

    public function indexDeletedAction() {
        $em = $this->getDoctrine()->getManager();

        $incidencias = $em->getRepository('AppBundle:Incidencia')->findAllDeletedIncidencias();



        return $this->render('incidencia/index.html.twig', array(
                    'incidencias' => $incidencias,
        ));
    }

    public function indexResolvedAction() {
        $em = $this->getDoctrine()->getManager();

        $incidencias = $em->getRepository('AppBundle:Incidencia')->findAllResolvedIncidencias();



        return $this->render('incidencia/index.html.twig', array(
                    'incidencias' => $incidencias,
        ));
    }

    /**
     * Creates a new Incidencia entity.
     *
     */
    public function newAction(Request $request) {
        $incidencium = new Incidencia();
        $form = $this->createForm('AppBundle\Form\IncidenciaType', $incidencium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comunidad = $incidencium->getComunidadId()[0];
            $incidencium->setComunidadId($comunidad);
            $telefonos = $incidencium->getTelefonos();
            $incidencium->setTelefonos(new ArrayCollection());
            foreach ($telefonos as $telefono) {
                if (empty($telefono->getTelefono())) {
                    $telefono->setIncidenciaId(null);
                    $em->remove($telefono);
                } else {
                    $incidencium->addTelefonos($telefono);
                }
            }
            $incidencium->setEliminado(false);
            $incidencium->setFechaCreacion(new \DateTime());
            $incidencium->setFechaResolucion(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($incidencium);
            $em->flush();

            return $this->redirectToRoute('incidencia_show', array('id' => $incidencium->getId()));
        }

        return $this->render('incidencia/new.html.twig', array(
                    'incidencium' => $incidencium,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Incidencia entity.
     *
     */
    public function showAction(Incidencia $incidencium) {
        $deleteForm = $this->createDeleteForm($incidencium);
        $telefonos = $incidencium->getTelefonos();
        $seguimientos = $incidencium->getSeguimientos();

        $deleteSeguimientoForms = array();
        foreach ($seguimientos as $seguimiento) {
            $deleteSeguimientoForms[$seguimiento->getId()] = $this->createDeleteSeguimientoForm($seguimiento)->createView();
        }


        return $this->render('incidencia/show.html.twig', array(
                    'incidencium' => $incidencium,
                    'delete_form' => $deleteForm->createView(),
                    'telefonos' => $telefonos,
                    'seguimientos' => $seguimientos,
                    'deleteSeguimientoForm' => $deleteSeguimientoForms
        ));
    }

    public function markAction(Incidencia $incidencium) {

        if ($incidencium->getMarcada()) {
            $incidencium->setMarcada(false);
        } else {
            $incidencium->setMarcada(true);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($incidencium);
        $em->flush();
        return $this->redirectToRoute('incidencia_index');
    }

    public function resolveAction(Incidencia $incidencium) {

        if ($incidencium->getResuelta()) {
            $incidencium->setResuelta(false);
        } else {
            $incidencium->setResuelta(true);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($incidencium);
        $em->flush();
        return $this->redirectToRoute('incidencia_index');
    }

    public function trashAction(Incidencia $incidencium) {

        if ($incidencium->getEliminado()) {
            $incidencium->setEliminado(false);
        } else {
            $incidencium->setEliminado(true);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($incidencium);
        $em->flush();
        return $this->redirectToRoute('incidencia_index');
    }

    /**
     * Displays a form to edit an existing Incidencia entity.
     *
     */
    public function editAction(Request $request, Incidencia $incidencium) {
        $deleteForm = $this->createDeleteForm($incidencium);
        $editForm = $this->createForm('AppBundle\Form\IncidenciaType', $incidencium);
        $editForm->handleRequest($request);



        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $comunidad = $incidencium->getComunidadId()[0];
            $telefonos = $incidencium->getTelefonos();
            $incidencium->setTelefonos(new ArrayCollection());
            foreach ($telefonos as $telefono) {
                if (empty($telefono->getTelefono())) {
                    $telefono->setIncidenciaId(null);
                    $em->remove($telefono);
                } else {
                    $incidencium->addTelefonos($telefono);
                }
            }
            $incidencium->setEliminado(false);
            $incidencium->setComunidadId($comunidad);
            $incidencium->setFechaModificacion(new \DateTime());
            $em->persist($incidencium);
            $em->flush();

            return $this->redirectToRoute('incidencia_index');
        }

        return $this->render('incidencia/edit.html.twig', array(
                    'incidencium' => $incidencium,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Incidencia entity.
     *
     */
    public function deleteAction(Request $request, Incidencia $incidencium) {
        $form = $this->createDeleteForm($incidencium);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($incidencium);
            $em->flush();
        }

        return $this->redirectToRoute('incidencia_index');
    }

    /**
     * Deletes a Incidencia entity.
     *
     */
    public function delAction($id) {
        $em = $this->getDoctrine()->getManager();
        $incidencium = $em->getRepository('AppBundle:Incidencia')->findOneById($id);
        $em->remove($incidencium);
        $em->flush();

        return $this->redirectToRoute('incidencia_index');
    }

    /**
     * Creates a form to delete a Incidencia entity.
     *
     * @param Incidencia $incidencium The Incidencia entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Incidencia $incidencia) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('incidencia_delete', array('id' => $incidencia->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    private function createDeleteSeguimientoForm(\AppBundle\Entity\Seguimiento $seguimiento) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('seguimiento_delete', array('id' => $seguimiento->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    public function addSeguimientoAction(Request $request, $id) {
        $seguimiento = new \AppBundle\Entity\Seguimiento();
        $form = $this->createForm('AppBundle\Form\SeguimientoType', $seguimiento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $incidencium = $em->getRepository('AppBundle:Incidencia')->findOneById($id);
            $seguimiento->setIncidenciaId($incidencium);
            $seguimiento->setFechaCreacion(new \DateTime());
            $em->persist($seguimiento);
            $em->flush();

            return $this->redirectToRoute('incidencia_show', array('id' => $id));
        }
        return $this->render('incidencia/addSeguimiento.html.twig', array(
                    'seguimiento' => $seguimiento,
                    'edit_form' => $form->createView(),
        ));
    }

    public function addAvisoAction(Request $request, $id) {
        $aviso = new \AppBundle\Entity\Aviso();
        $em = $this->getDoctrine()->getManager();
        $incidencium = $em->getRepository('AppBundle:Incidencia')->findOneById($id);
        $description = 'Expediente: ' . $incidencium->getExpediente() . ' DESCRIPCION: ' . $incidencium->getDescripcion();
        $aviso->setDescripcion($description);
        $aviso->setNombre($incidencium->getBreve());
        $aviso->setPrioridad($incidencium->getPrioridad());
        $aviso->setIncidenciaId($incidencium);
        $aviso->setEliminado(false);
        $aviso->setMarcada(false);
        $aviso->setFechaAviso(new \DateTime());
        $aviso->setFechaCreacion(new \DateTime());
        $em->persist($aviso);
        $em->flush();
        return $this->redirectToRoute('incidencia_index');
    }

}
