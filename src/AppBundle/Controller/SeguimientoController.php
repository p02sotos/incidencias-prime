<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Seguimiento;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * Seguimiento controller.
 *
 */
class SeguimientoController extends Controller
{
    /**
     * Lists all seguimiento entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $seguimientos = $em->getRepository('AppBundle:Seguimiento')->findAll();

        return $this->render('seguimiento/index.html.twig', array(
            'seguimientos' => $seguimientos,
        ));
    }

    /**
     * Creates a new seguimiento entity.
     *
     */
    public function newAction(Request $request)
    {
        $seguimiento = new Seguimiento();
        $form = $this->createForm('AppBundle\Form\SeguimientoType', $seguimiento);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($seguimiento);
            $em->flush($seguimiento);

            return $this->redirectToRoute('seguimiento_show', array('id' => $seguimiento->getId()));
        }

        return $this->render('seguimiento/new.html.twig', array(
            'seguimiento' => $seguimiento,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a seguimiento entity.
     *
     */
    public function showAction(Seguimiento $seguimiento)
    {
        $deleteForm = $this->createDeleteForm($seguimiento);

        return $this->render('seguimiento/show.html.twig', array(
            'seguimiento' => $seguimiento,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing seguimiento entity.
     *
     */
    public function editAction(Request $request, Seguimiento $seguimiento)
    {
        $deleteForm = $this->createDeleteForm($seguimiento);
        $editForm = $this->createForm('AppBundle\Form\SeguimientoType', $seguimiento);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('seguimiento_edit', array('id' => $seguimiento->getId()));
        }

        return $this->render('seguimiento/edit.html.twig', array(
            'seguimiento' => $seguimiento,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a seguimiento entity.
     *
     */
    public function deleteAction(Request $request, Seguimiento $seguimiento)
    {
        $form = $this->createDeleteForm($seguimiento);
        $form->handleRequest($request);
        $referer = $request->headers->get('referer');       


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($seguimiento);
            $em->flush($seguimiento);
        }

        return new RedirectResponse($referer);
    }

    /**
     * Creates a form to delete a seguimiento entity.
     *
     * @param Seguimiento $seguimiento The seguimiento entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Seguimiento $seguimiento)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('seguimiento_delete', array('id' => $seguimiento->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
