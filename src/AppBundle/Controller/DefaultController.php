<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Form\IncidenciaType;
use AppBundle\Entity\Incidencia;
use AppBundle\Entity\Comunidad;
use AppBundle\Form\ComunidadType;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {
        
        
        
        return $this->redirectToRoute('incidencia_index');
    }

    /**
     * @Route("/createIncidencia", name="createIncidencia")
     * @Method({"GET", "POST"})
     * 
     */
    public function createIncidenciaAction(Request $request) {
        $incidencia = new Incidencia();
        $form = $this->createForm(IncidenciaType::class, $incidencia);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $incidencia = $form->getData();
            if ($incidencia->getComunidadId()) {

                $comunidad = $incidencia->getComunidadId()[0];
                $incidencia->setComunidadId($comunidad);
                $incidencia->setFechaCreacion(new \DateTime());
                $incidencia->setFechaResolucion(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($incidencia);
                $em->flush();
            } else {
                return $this->render('default/pages/createIncidencia.html.twig', array(
                            'formI' => $form->createView(),
                ));
            }
            return $this->redirectToRoute('homepage');
        }
        return $this->render('default/pages/createIncidencia.html.twig', array(
                    'formI' => $form->createView(),
        ));
    }

    /**
     * @Route("/createComunidad", name="createComunidad")
     * @Method({"GET", "POST"})
     * 
     */
    public function createComunidadAction(Request $request) {
        $comunidad = new Comunidad();
        $form = $this->createForm(ComunidadType::class, $comunidad);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $comunidad = $form->getData();
            $comunidad->setFechaCreacion(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($comunidad);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('default/pages/createComunidad.html.twig', array(
                    'formC' => $form->createView(),
        ));
    }

    /**
     * @Route("/editComunidad/{id}", name="editComunidad", options={"expose"=true})
     * @Method({"GET", "PUT"})
     */
    public function editComunidadAction(Request $request, Comunidad $comunidad) {

        $form = $this->createForm(ComunidadType::class, $comunidad, array(
            'method' => 'PUT',
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comunidad = $form->getData();
            $comunidad->setFechaModificacion(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($comunidad);
            $em->flush();
            return $this->redirectToRoute('homepage');
        }
        return $this->render('default/pages/createComunidad.html.twig', array(
                    'formC' => $form->createView(),
        ));
    }

    /**
     * @Route("/editIncidencia/{id}", name="editIncidencia", options={"expose"=true})
     * @Method({"GET", "PUT"})
     */
    public function editIncidenciaAction(Request $request, Incidencia $incidencia) {

        $form = $this->createForm(IncidenciaType::class, $incidencia, array(
            'method' => 'PUT',
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $incidencia = $form->getData();
            if ($incidencia->getComunidadId()) {
                $comunidad = $incidencia->getComunidadId()[0];
                $incidencia->setComunidadId($comunidad);
                $incidencia->setFechaModificacion(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($incidencia);
                $em->flush();
            } else {
                return $this->render('default/pages/createIncidencia.html.twig', array(
                            'formI' => $form->createView(),
                ));
            }
            return $this->redirectToRoute('homepage');
        }
        return $this->render('default/pages/createIncidencia.html.twig', array(
                    'formI' => $form->createView(),
        ));
    }

}
