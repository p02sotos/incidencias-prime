<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Tarea;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tarea controller.
 *
 */
class TareaController extends Controller {

    /**
     * Lists all tarea entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $tareas = $em->getRepository('AppBundle:Tarea')->findAllUnresolvedTareas();

        return $this->render('tarea/index.html.twig', array(
                    'tareas' => $tareas,
        ));
    }

    /**
     * Lists all deleted aviso entities.
     *
     */
    public function indexDeletedAction() {
        $em = $this->getDoctrine()->getManager();
        $tareas = $em->getRepository('AppBundle:Tarea')->findAllDeletedTareas();
        $retriever = $this->get('app.retrieve_last_data');
        $retriever->setDataToSession();

        return $this->render('tarea/index.html.twig', array(
                    'tareas' => $tareas,
        ));
    }

    /**
     * Lists all resolved aviso entities.
     *
     */
    public function indexResolvedAction() {
        $em = $this->getDoctrine()->getManager();
        $tareas = $em->getRepository('AppBundle:Tarea')->findAllResolvedTareas();
        $retriever = $this->get('app.retrieve_last_data');
        $retriever->setDataToSession();
        return $this->render('tarea/index.html.twig', array(
                    'tareas' => $tareas,
        ));
    }

    /**
     * Creates a new tarea entity.
     *
     */
    public function newAction(Request $request) {
        $tarea = new Tarea();
        $form = $this->createForm('AppBundle\Form\TareaType', $tarea);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tarea);
            $em->flush($tarea);

            return $this->redirectToRoute('tarea_show', array('id' => $tarea->getId()));
        }

        return $this->render('tarea/new.html.twig', array(
                    'tarea' => $tarea,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a tarea entity.
     *
     */
    public function showAction(Tarea $tarea) {
        $deleteForm = $this->createDeleteForm($tarea);

        return $this->render('tarea/show.html.twig', array(
                    'tarea' => $tarea,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing tarea entity.
     *
     */
    public function editAction(Request $request, Tarea $tarea) {
        $deleteForm = $this->createDeleteForm($tarea);
        $editForm = $this->createForm('AppBundle\Form\TareaType', $tarea);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tarea_index');
        }

        return $this->render('tarea/edit.html.twig', array(
                    'tarea' => $tarea,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tarea entity.
     *
     */
    public function deleteAction(Request $request, Tarea $tarea) {
        $form = $this->createDeleteForm($tarea);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tarea);
            $em->flush($tarea);
        }

        return $this->redirectToRoute('tarea_index');
    }

    /**
     * Creates a form to delete a tarea entity.
     *
     * @param Tarea $tarea The tarea entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Tarea $tarea) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('tarea_delete', array('id' => $tarea->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    public function resolveAction(Request $request, Tarea $tarea) {

        $marcada = $tarea->getRealizada();

        if ($marcada) {
            $tarea->setRealizada(false);
        } else {
            $tarea->setRealizada(true);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($tarea);
        $em->flush();
        $retriever = $this->get('app.retrieve_last_data');
        $retriever->setDataToSession();

        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

    public function trashAction(Request $request, Tarea $tarea) {

        if ($tarea->getEliminado()) {
            $tarea->setEliminado(false);
        } else {
            $tarea->setEliminado(true);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($tarea);
        $em->flush();
        $retriever = $this->get('app.retrieve_last_data');
        $retriever->setDataToSession();
        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

    public function markAction(Tarea $tarea) {

        if ($tarea->getMarcada()) {
            $tarea->setMarcada(false);
        } else {
            $tarea->setMarcada(true);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($tarea);
        $em->flush();
        return $this->redirectToRoute('tarea_index');
    }

}
