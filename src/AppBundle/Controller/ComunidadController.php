<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comunidad;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Comunidad controller.
 *
 */
class ComunidadController extends Controller
{
    /**
     * Lists all comunidad entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $comunidads = $em->getRepository('AppBundle:Comunidad')->findAll();

        return $this->render('comunidad/index.html.twig', array(
            'comunidads' => $comunidads,
        ));
    }

    /**
     * Creates a new comunidad entity.
     *
     */
    public function newAction(Request $request)
    {
        $comunidad = new Comunidad();
        $form = $this->createForm('AppBundle\Form\ComunidadType', $comunidad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comunidad);
            $em->flush($comunidad);

            return $this->redirectToRoute('comunidad_show', array('id' => $comunidad->getId()));
        }

        return $this->render('comunidad/new.html.twig', array(
            'comunidad' => $comunidad,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a comunidad entity.
     *
     */
    public function showAction(Comunidad $comunidad)
    {
        $deleteForm = $this->createDeleteForm($comunidad);

        return $this->render('comunidad/show.html.twig', array(
            'comunidad' => $comunidad,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing comunidad entity.
     *
     */
    public function editAction(Request $request, Comunidad $comunidad)
    {
        $deleteForm = $this->createDeleteForm($comunidad);
        $editForm = $this->createForm('AppBundle\Form\ComunidadType', $comunidad);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('comunidad_edit', array('id' => $comunidad->getId()));
        }

        return $this->render('comunidad/edit.html.twig', array(
            'comunidad' => $comunidad,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a comunidad entity.
     *
     */
    public function deleteAction(Request $request, Comunidad $comunidad)
    {
        $form = $this->createDeleteForm($comunidad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($comunidad);
            $em->flush($comunidad);
        }

        return $this->redirectToRoute('comunidad_index');
    }

    /**
     * Creates a form to delete a comunidad entity.
     *
     * @param Comunidad $comunidad The comunidad entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Comunidad $comunidad)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('comunidad_delete', array('id' => $comunidad->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
