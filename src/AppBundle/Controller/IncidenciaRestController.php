<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Context\Context;

class IncidenciaRestController extends FOSRestController {

    /**
     * Get all the incidencias
     * @return array 
     * @View
     */
    public function getIncidenciasAction() {

        $em = $this->getDoctrine()->getManager();
        $indicencias = $em->getRepository('AppBundle:Incidencia')->findAllIncidencias();
        $view = new \FOS\RestBundle\View\View();
        $view->setData($indicencias);
        $context = new Context();
        $context->setVersion('2.1');
        $context->setGroups(array('me'));
        $view->setContext($context);
        return $this->handleView($view);
    }

    /**
     * Get all the incidencias
     * @return array 
     * 
     * @View
     */
    public function postIncidenciasAction(Request $request) {
        $entity = new \AppBundle\Entity\Incidencia();
        $defaultData = array('message' => 'Type your message here');
        $form = $this->createFormBuilder($defaultData)
                ->add('name', TextType::class)
                ->add('email', EmailType::class)
                ->add('message', TextareaType::class)
                ->add('send', SubmitType::class)
                ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            // data is an array with "name", "email", and "message" keys
            $data = $form->getData();
        }
    }

    /**
     * Get all the incidencias
     * @return array 
     * @View
     */
    public function getComunidadesAction() {

        $em = $this->getDoctrine()->getManager();
        $comunidades = $em->getRepository('AppBundle:Comunidad')->findAllComunidades();
        $view = new \FOS\RestBundle\View\View();
        $view->setData($comunidades);

        return $this->handleView($view);
    }

    /**
     * Get all the comunidades with 
     * @return array 
     * @View
     */
    public function getComunidadesminAction() {

        $em = $this->getDoctrine()->getManager();
        $comunidades = $em->getRepository('AppBundle:Comunidad')->findAllComunidadesMin();
        $view = new \FOS\RestBundle\View\View();
        $view->setData($comunidades);

        return $this->handleView($view);
    }

}
