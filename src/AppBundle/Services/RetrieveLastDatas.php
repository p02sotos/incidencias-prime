<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Services;

/**
 * Description of RetrieveLastDatas
 *
 * @author Ser
 */
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;

class RetrieveLastDatas {

    protected $em;
    protected $session;
    private $avisos;
    private $tareas;

    public function __construct(EntityManager $entityManager, Session $session) {
        $this->em = $entityManager;
        $this->session = $session;
        $this->avisos = $this->em->getRepository('AppBundle:Aviso')->getLastAvisos(20);
        $this->tareas = $this->em->getRepository('AppBundle:Tarea')->getLastTareas(20);
    }

    public function getAvisos() {
        $this->avisos = $this->em->getRepository('AppBundle:Aviso')->getLastAvisos(20);
        return $this->avisos;
    }

    public function getTareas() {
        $this->tareas = $this->em->getRepository('AppBundle:Tarea')->getLastTareas(20);
        return $this->tareas;
    }

    public function getDatas() {
        $data = array();
        $data['avisos'] = $this->getAvisos();
        $data['tareas'] = $this->getTareas();
        return $data;
    }

    public function setDataToSession() {
        $this->session->set('Avisos', $this->getAvisos());
        $this->session->set('Tareas', $this->getTareas());
    }

}
