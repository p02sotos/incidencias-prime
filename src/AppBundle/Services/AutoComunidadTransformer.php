<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AutoComunidadTransformer
 *
 * @author Ser
 */

namespace AppBundle\Services;

use AppBundle\Entity\Comunidad;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class AutoComunidadTransformer implements DataTransformerInterface {

    private $manager;

    public function __construct(ObjectManager $manager) {
        $this->manager = $manager;
    }

    /**
     * Transforms a string (nombre) to an object (comunidad).
     *
     * @param  string $comunidadName
     * @return Comunidad|null
     * @throws TransformationFailedException if object (comunidad) is not found.
     */
    public function reverseTransform($comunidadName) {
        if (!$comunidadName) {
            return;
        }
        $comunidad = $this->manager
                ->getRepository('AppBundle:Comunidad')
                // query for the issue with this id
                ->findByNombre($comunidadName);

        if (null === $comunidad || empty($comunidad)) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                    'An issue with number "%s" does not exist!', $comunidadName
            ));
        }

        return $comunidad;
    }

    /**
     * Transforms an object (comunidad) to a string (nombre).
     *
     * @param  Comunidad|null $comunidad
     * @return string
     */
    public function transform($comunidad) {
        if (null === $comunidad) {
            return '';
        }

        return $comunidad->getNombre();
    }

}
