<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Services\AutoComunidadTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;


class IncidenciaType extends AbstractType {

    private $manager;

    public function __construct(ObjectManager $manager) {
        $this->manager = $manager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('origen', TextType::class, array(
                    'required' => false
                ))
                ->add('nombrePersona', TextType::class, array(
                    'required' => false
                ))
                ->add('breve')
                ->add('expediente', TextType::class, array(
                    'required' => false
                ))
               
                ->add('descripcion')
                 ->add('atencion', TextType::class, array(
                    'required' => false
                ))
                ->add('prioridad')
                ->add('fechaIncidencia', DateType::class, array(
                    'widget' => 'single_text',
                    'html5' => false,
                    'attr' => ['class' => 'js-datepicker']))
                ->add('telefonos', CollectionType::class, array(
                    'entry_type' => TelefonoType::class,
                     'allow_add'    => true,
                    'prototype' => true,
                    'label' => false
                ))
                ->add('comunidadId', TextType::class, array(
                    'invalid_message' => 'That is not a valid comunidad name',
                    'attr' => ['class' => 'remote']
                ))

        ;
        $builder->get('comunidadId')
                ->addModelTransformer(new AutoComunidadTransformer($this->manager));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Incidencia'
        ));
    }

}
