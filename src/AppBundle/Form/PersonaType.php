<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Services\AutoComunidadTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PersonaType extends AbstractType {

    private $manager;

    public function __construct(ObjectManager $manager) {
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('nombre')
                ->add('direccion', TextType::class, array(
                    'required' => false
                ))
                ->add('nota', TextareaType::class, array(
                    'required' => false,
                    'attr' => ['rows' => '20',
                        'cols' => '50']))
                ->add('comunidadId', TextType::class, array(
                    'invalid_message' => 'That is not a valid comunidad name',
                    'attr' => ['class' => 'remote']
        ));

        $builder->get('comunidadId')
                ->addModelTransformer(new AutoComunidadTransformer($this->manager));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Persona'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'appbundle_persona';
    }

}
