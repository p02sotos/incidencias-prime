<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of InjectGlobalDataListener
 *
 * @author Ser
 */

namespace AppBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;


class InjectGlobalDataListener {

    protected $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function onKernelController(FilterControllerEvent $event) {
        $session = $event->getRequest()->getSession();
        if (!$session->get('Avisos') || !$session->get('Tareas')) {
            $retrievedLastData = $this->container->get('app.retrieve_last_data');
            $session->set('Avisos', $retrievedLastData->getAvisos());
            $session->set('Tareas', $retrievedLastData->getTareas());
        }
    }

}
