$('#dlg').puidialog({
            showEffect: 'fade',
            hideEffect: 'fade',
            minimizable: true,
            maximizable: true,
            responsive: true,
            minWidth: 600,
            width: 600,
            buttons: [{
                    text: 'Yes',
                    icon: 'fa-check',
                    click: function () {
                        $('#dlg').puidialog('hide');
                    }
                },
                {
                    text: 'No',
                    icon: 'fa-close',
                    click: function () {
                        $('#dlg').puidialog('hide');
                    }
                }
            ]
        });