$('#tbl').puidatatable({
            caption: 'Incidencias',
            paginator: {
                rows: 30
            },
            globalFilter: '#globalFilter',
            columns: [
                {rowToggler: true, bodyStyle: 'width:35px', headerStyle: 'width:35px'},
                {field: 'i_id', headerText: 'Id', sortable: true, bodyStyle: 'width:50px', headerStyle: 'width:50px'},
                {field: 'i_breve', headerText: 'Descripción', filter: true, sortable: true, bodyStyle: 'width:40%', headerStyle: 'width:40%'},
                {field: 'i_expediente', headerText: 'nº Expediente', filter: true, bodyStyle: 'width:120px', headerStyle: 'width:120px', sortable: true},
                {field: 'i_descripcion', headerText: 'nº Expediente', filter: true, bodyStyle: 'display: none', headerStyle: 'display: none', sortable: true},
                {field: 'i_fechaIncidencia', headerText: 'Fecha', filter: true, bodyStyle: 'width:120px', headerStyle: 'width:120px', sortable: true, content: function (rowData) {
                        return $.format.date(rowData.i_fechaIncidencia, "dd/MM/yyyy");
                    }
                },
                {field: 'cNombre', headerText: 'Comunidad', filter: true, sortable: true},
                {field: 'pNombre', headerText: 'Persona', filter: true, sortable: true}

            ],
            rowSelect: function (event, data) {
                incidenciaId = data.i_id;
            },
            rowUnselect: function (event, data) {
                incidenciaId = null;
            },
            expandableRows: true,
            rowExpandMode: 'single',
            expandedRowContent: function (data) {

                return $('<div class="ui-grid ui-grid-responsive"></div>')
                        .append('<div class="ui-grid-row"><div class="ui-grid-col-1">Breve:</div> <div class="ui-grid-col-11" style="word-break:break-word; white-space: normal;">' + data.i_breve + '</div></div>')
                        .append('<div class="ui-grid-row"><div class="ui-grid-col-1">Descripción:</div> <div class="ui-grid-col-11" style="word-break:break-word; white-space: normal;">' + data.i_descripcion + '</div></div>')
                        .append('<div class="ui-grid-row"><div class="ui-grid-col-1">Atención:</div> <div class="ui-grid-col-11" style="word-break:break-word; white-space: normal;">' + data.i_atencion + '</div></div>')
                        .append('<div class="ui-grid-row"><div class="ui-grid-col-1">Origen:</div> <div class="ui-grid-col-11" style="word-break:break-word; white-space: normal;">' + data.i_origen + '</div></div>')
                        .append('<div class="ui-grid-row"><div class="ui-grid-col-1">Expediente:</div> <div class="ui-grid-col-11" style="word-break:break-word; white-space: normal;">' + data.i_expediente + '</div></div>')


                        ;
            },
            selectionMode: 'single',
            resizableColumns: true,
            datasource: function (callback) {
                $.ajax({
                    type: "GET",
                    url: 'http://localhost/api-inc/web/app_dev.php/api/incidencias',
                    dataType: "json",
                    contentType: "charset=utf-8",
                    context: this,
                    success: function (response) {
                        data = response;
                        callback.call(this, response);
                    }
                });
            }
        });