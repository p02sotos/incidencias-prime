<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Description of IncidenciaResository
 *
 * @author Ser
 */
class AvisoRepository extends EntityRepository {

    //put your code here

    public function findAllUnresolvedAvisos() {
        $query = $this->getEntityManager()
                ->createQuery(
                'SELECT a, i FROM AppBundle:Aviso a
             LEFT JOIN a.incidenciaId i WHERE a.marcada = false 
             AND a.eliminado = false
            
            '
        );

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function findAllResolvedAvisos() {
        $query = $this->getEntityManager()
                ->createQuery(
                'SELECT a, i FROM AppBundle:Aviso a
             LEFT JOIN a.incidenciaId i WHERE a.marcada = true
             AND a.eliminado = false
            
            '
        );

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
     public function findAllDeletedAvisos() {
        $query = $this->getEntityManager()
                ->createQuery(
                'SELECT a, i FROM AppBundle:Aviso a
             LEFT JOIN a.incidenciaId i WHERE a.eliminado = true
            
            
            '
        );

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function findAllAvisos() {
        $query = $this->getEntityManager()
                ->createQuery(
                'SELECT a, i FROM AppBundle:Aviso a
             LEFT JOIN a.incidenciaId i 
            
            '
        );

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function getLastAvisos($limit) {

        $query = $this->createQueryBuilder('a')
                ->where('a.marcada = false')
                ->andWhere('a.eliminado = false')
                ->orderBy('a.fechaAviso', 'DESC')
                ->setMaxResults($limit)
                ->getQuery();
        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

}
