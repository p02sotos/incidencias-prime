<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tarea
 *
 * @ORM\Table(name="tarea", indexes={@ORM\Index(name="tarea_FI_1", columns={"incidencia_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\TareaRepository")
 */
class Tarea
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=1024, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", length=65535, nullable=true)
     */
    private $descripcion;

    /**
     * @var integer
     *
     * @ORM\Column(name="prioridad", type="integer", nullable=false)
     */
    private $prioridad;

    /**
     * @var integer
     *
     * @ORM\Column(name="orden", type="integer", nullable=true)
     */
    private $orden;

    /**
     * @var boolean
     *
     * @ORM\Column(name="marcada", type="boolean", nullable=true)
     */
    private $marcada;

    /**
     * @var boolean
     *
     * @ORM\Column(name="eliminado", type="boolean", nullable=true)
     */
    private $eliminado;

    /**
     * @var boolean
     *
     * @ORM\Column(name="realizada", type="boolean", nullable=true)
     */
    private $realizada;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_tarea", type="date", nullable=true)
     */
    private $fechaTarea;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="date", nullable=true)
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_modificacion", type="date", nullable=true)
     */
    private $fechaModificacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="incidencia_id", type="integer", nullable=true)
     */
    private $incidenciaId;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Tarea
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Tarea
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set prioridad
     *
     * @param integer $prioridad
     *
     * @return Tarea
     */
    public function setPrioridad($prioridad)
    {
        $this->prioridad = $prioridad;

        return $this;
    }

    /**
     * Get prioridad
     *
     * @return integer
     */
    public function getPrioridad()
    {
        return $this->prioridad;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     *
     * @return Tarea
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set marcada
     *
     * @param boolean $marcada
     *
     * @return Tarea
     */
    public function setMarcada($marcada)
    {
        $this->marcada = $marcada;

        return $this;
    }

    /**
     * Get marcada
     *
     * @return boolean
     */
    public function getMarcada()
    {
        return $this->marcada;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     *
     * @return Tarea
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return boolean
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }

    /**
     * Set realizada
     *
     * @param boolean $realizada
     *
     * @return Tarea
     */
    public function setRealizada($realizada)
    {
        $this->realizada = $realizada;

        return $this;
    }

    /**
     * Get realizada
     *
     * @return boolean
     */
    public function getRealizada()
    {
        return $this->realizada;
    }

    /**
     * Set fechaTarea
     *
     * @param \DateTime $fechaTarea
     *
     * @return Tarea
     */
    public function setFechaTarea($fechaTarea)
    {
        $this->fechaTarea = $fechaTarea;

        return $this;
    }

    /**
     * Get fechaTarea
     *
     * @return \DateTime
     */
    public function getFechaTarea()
    {
        return $this->fechaTarea;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return Tarea
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaModificacion
     *
     * @param \DateTime $fechaModificacion
     *
     * @return Tarea
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * Set incidenciaId
     *
     * @param integer $incidenciaId
     *
     * @return Tarea
     */
    public function setIncidenciaId($incidenciaId)
    {
        $this->incidenciaId = $incidenciaId;

        return $this;
    }

    /**
     * Get incidenciaId
     *
     * @return integer
     */
    public function getIncidenciaId()
    {
        return $this->incidenciaId;
    }
}
