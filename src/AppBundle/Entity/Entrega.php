<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entrega
 *
 * @ORM\Table(name="entrega", indexes={@ORM\Index(name="entrega_FI_1", columns={"comunidad_id"}), @ORM\Index(name="entrega_FI_2", columns={"tipo_id"})})
 * @ORM\Entity
 */
class Entrega
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=1024, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", length=65535, nullable=true)
     */
    private $descripcion;

    /**
     * @var integer
     *
     * @ORM\Column(name="priodidad", type="integer", nullable=true)
     */
    private $priodidad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="marcada", type="boolean", nullable=true)
     */
    private $marcada;

    /**
     * @var boolean
     *
     * @ORM\Column(name="eliminado", type="boolean", nullable=true)
     */
    private $eliminado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_entrega", type="date", nullable=true)
     */
    private $fechaEntrega;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="date", nullable=true)
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_modificacion", type="date", nullable=true)
     */
    private $fechaModificacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="comunidad_id", type="integer", nullable=true)
     */
    private $comunidadId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo_id", type="integer", nullable=true)
     */
    private $tipoId;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Entrega
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Entrega
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set priodidad
     *
     * @param integer $priodidad
     *
     * @return Entrega
     */
    public function setPriodidad($priodidad)
    {
        $this->priodidad = $priodidad;

        return $this;
    }

    /**
     * Get priodidad
     *
     * @return integer
     */
    public function getPriodidad()
    {
        return $this->priodidad;
    }

    /**
     * Set marcada
     *
     * @param boolean $marcada
     *
     * @return Entrega
     */
    public function setMarcada($marcada)
    {
        $this->marcada = $marcada;

        return $this;
    }

    /**
     * Get marcada
     *
     * @return boolean
     */
    public function getMarcada()
    {
        return $this->marcada;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     *
     * @return Entrega
     */
    public function setEliminado($eliminado)
    {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return boolean
     */
    public function getEliminado()
    {
        return $this->eliminado;
    }

    /**
     * Set fechaEntrega
     *
     * @param \DateTime $fechaEntrega
     *
     * @return Entrega
     */
    public function setFechaEntrega($fechaEntrega)
    {
        $this->fechaEntrega = $fechaEntrega;

        return $this;
    }

    /**
     * Get fechaEntrega
     *
     * @return \DateTime
     */
    public function getFechaEntrega()
    {
        return $this->fechaEntrega;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return Entrega
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaModificacion
     *
     * @param \DateTime $fechaModificacion
     *
     * @return Entrega
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * Set comunidadId
     *
     * @param integer $comunidadId
     *
     * @return Entrega
     */
    public function setComunidadId($comunidadId)
    {
        $this->comunidadId = $comunidadId;

        return $this;
    }

    /**
     * Get comunidadId
     *
     * @return integer
     */
    public function getComunidadId()
    {
        return $this->comunidadId;
    }

    /**
     * Set tipoId
     *
     * @param integer $tipoId
     *
     * @return Entrega
     */
    public function setTipoId($tipoId)
    {
        $this->tipoId = $tipoId;

        return $this;
    }

    /**
     * Get tipoId
     *
     * @return integer
     */
    public function getTipoId()
    {
        return $this->tipoId;
    }
}
