<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Description of IncidenciaResository
 *
 * @author Ser
 */
class TareaRepository extends EntityRepository {

  
    
   public function getLastTareas($limit) {

        $query = $this->createQueryBuilder('t')
                ->orderBy('t.fechaTarea', 'DESC')
                ->where('t.eliminado = false')
                ->andWhere('t.realizada = false')
                ->orderBy('t.fechaTarea', 'ASC')
                ->setMaxResults($limit)
                ->getQuery();
        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
     public function findAllUnresolvedTareas() {
        $query = $this->getEntityManager()
                ->createQuery(
                'SELECT t  FROM AppBundle:Tarea t
              WHERE 
             t.eliminado = false
             AND t.realizada = false
            
            '
        );

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
     public function findAllResolvedTareas() {
        $query = $this->getEntityManager()
                ->createQuery(
                'SELECT t FROM AppBundle:Tarea t
              WHERE 
             t.eliminado = false
             AND t.realizada = true
            
            '
        );

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function findAllDeletedTareas() {
        $query = $this->getEntityManager()
                ->createQuery(
             '
             SELECT t FROM AppBundle:Tarea t
              WHERE 
             t.eliminado = true       
            '
        );

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }


}
