<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * Persona
 *
 * @ORM\Table(name="persona", indexes={@ORM\Index(name="persona_FI_1", columns={"comunidad_id"})})
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Persona {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     * @Groups({"list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=128, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=128, nullable=true)
     */
    private $direccion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="marcada", type="boolean", nullable=true)
     */
    private $marcada;

    /**
     * @var string
     *
     * @ORM\Column(name="nota", type="text", length=65535, nullable=true)
     */
    private $nota;

    /**
     * @ORM\ManyToOne(targetEntity="Comunidad", inversedBy="personas")
     * @ORM\JoinColumn(name="comunidad_id", referencedColumnName="id")
     */
    private $comunidadId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="date", nullable=true)
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_modificacion", type="date", nullable=true)
     */
    private $fechaModificacion;

    /**
     * @ORM\OneToMany(targetEntity="Incidencia", mappedBy="personaId")
     */
    private $incidencias;

    public function __construct() {
        $this->incidencias = new ArrayCollection();
    }

    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getMarcada() {
        return $this->marcada;
    }

    function getNota() {
        return $this->nota;
    }

    function getComunidadId() {
        return $this->comunidadId;
    }

    function getFechaCreacion() {
        return $this->fechaCreacion;
    }

    function getFechaModificacion() {
        return $this->fechaModificacion;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setMarcada($marcada) {
        $this->marcada = $marcada;
    }

    function setNota($nota) {
        $this->nota = $nota;
    }

    function setComunidadId($comunidadId) {
        $this->comunidadId = $comunidadId;
    }

    function setFechaCreacion(\DateTime $fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;
    }

    function setFechaModificacion(\DateTime $fechaModificacion) {
        $this->fechaModificacion = $fechaModificacion;
    }

    public function __toString() {
        return $this->nombre;
    }

}
