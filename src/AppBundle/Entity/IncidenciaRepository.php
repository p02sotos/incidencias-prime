<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Description of IncidenciaResository
 *
 * @author Ser
 */
class IncidenciaRepository extends EntityRepository {

    //put your code here

    public function findAllIncidencias() {
        $query = $this->getEntityManager()
                ->createQuery(
                'SELECT i, i.id as i_id, c.id as c_id,c.nombre as c_nombre,  p.id as p_id,  p.nombre as p_nombre, t FROM AppBundle:Incidencia i
             LEFT JOIN i.comunidadId c
             LEFT JOIN i.personaId p
             LEFT JOIN i.telefonos t
             WHERE i.eliminado = false
             AND i.resuelta = false
            
            '
        );

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function findAllResolvedIncidencias() {
        $query = $this->getEntityManager()
                ->createQuery(
                'SELECT i, i.id as i_id, c.id as c_id,c.nombre as c_nombre,  p.id as p_id,  p.nombre as p_nombre, t FROM AppBundle:Incidencia i
             LEFT JOIN i.comunidadId c
             LEFT JOIN i.personaId p
             LEFT JOIN i.telefonos t
             WHERE i.eliminado = false
             AND i.resuelta = true
            
            '
        );

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function findAllDeletedIncidencias() {
        $query = $this->getEntityManager()
                ->createQuery(
                'SELECT i, i.id as i_id, c.id as c_id,c.nombre as c_nombre,  p.id as p_id,  p.nombre as p_nombre, t FROM AppBundle:Incidencia i
             LEFT JOIN i.comunidadId c
             LEFT JOIN i.personaId p
             LEFT JOIN i.telefonos t
             WHERE i.eliminado = true
            
            
            '
        );

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

}
