<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Telefono
 *
 * @ORM\Table(name="telefono", indexes={@ORM\Index(name="telefono_FI_1", columns={"persona_id"}), @ORM\Index(name="telefono_FI_2", columns={"incidencia_id"}), @ORM\Index(name="telefono_FI_3", columns={"empresa_id"})})
 * @ORM\Entity
 */
class Telefono
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=128, nullable=false)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=128, nullable=true)
     */
    private $tipo = 'CASA';

    /**
     * @var string
     *
     * @ORM\Column(name="nota", type="text", length=65535, nullable=true)
     */
    private $nota;

    /**
     * @var boolean
     *
     * @ORM\Column(name="marcada", type="boolean", nullable=true)
     */
    private $marcada = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="persona_id", type="integer", nullable=true)
     */
    private $personaId;

    

    /**
     * @var integer
     *
     * @ORM\Column(name="empresa_id", type="integer", nullable=true)
     */
    private $empresaId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="date", nullable=true)
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_modificacion", type="date", nullable=true)
     */
    private $fechaModificacion;
    
    /**
     * 
     * @ORM\ManyToOne(targetEntity="Incidencia", inversedBy="telefonos")
     * @ORM\JoinColumn(name="incidencia_id", referencedColumnName="id")
     * 
     */
    private $incidenciaId;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Telefono
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set nota
     *
     * @param string $nota
     *
     * @return Telefono
     */
    public function setNota($nota)
    {
        $this->nota = $nota;

        return $this;
    }

    /**
     * Get nota
     *
     * @return string
     */
    public function getNota()
    {
        return $this->nota;
    }

    /**
     * Set marcada
     *
     * @param boolean $marcada
     *
     * @return Telefono
     */
    public function setMarcada($marcada)
    {
        $this->marcada = $marcada;

        return $this;
    }

    /**
     * Get marcada
     *
     * @return boolean
     */
    public function getMarcada()
    {
        return $this->marcada;
    }

    /**
     * Set personaId
     *
     * @param integer $personaId
     *
     * @return Telefono
     */
    public function setPersonaId($personaId)
    {
        $this->personaId = $personaId;

        return $this;
    }

    /**
     * Get personaId
     *
     * @return integer
     */
    public function getPersonaId()
    {
        return $this->personaId;
    }

    /**
     * Set incidenciaId
     *
     * @param integer $incidenciaId
     *
     * @return Telefono
     */
    public function setIncidenciaId($incidenciaId)
    {
        $this->incidenciaId = $incidenciaId;

        return $this;
    }

    /**
     * Get incidenciaId
     *
     * @return integer
     */
    public function getIncidenciaId()
    {
        return $this->incidenciaId;
    }

    /**
     * Set empresaId
     *
     * @param integer $empresaId
     *
     * @return Telefono
     */
    public function setEmpresaId($empresaId)
    {
        $this->empresaId = $empresaId;

        return $this;
    }

    /**
     * Get empresaId
     *
     * @return integer
     */
    public function getEmpresaId()
    {
        return $this->empresaId;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return Telefono
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaModificacion
     *
     * @param \DateTime $fechaModificacion
     *
     * @return Telefono
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }
    
}
