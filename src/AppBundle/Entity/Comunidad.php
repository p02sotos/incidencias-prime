<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Comunidad
 *
 * @ORM\Table(name="comunidad")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ComunidadRepository")
 */
class Comunidad {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=128, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=1028, nullable=true)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="presidente", type="string", length=128, nullable=true)
     */
    private $presidente;

    /**
     * @var string
     *
     * @ORM\Column(name="cif", type="string", length=128, nullable=true)
     */
    private $cif;

    /**
     * @var string
     *
     * @ORM\Column(name="cuenta", type="string", length=512, nullable=true)
     */
    private $cuenta;

    /**
     * @var boolean
     *
     * @ORM\Column(name="marcada", type="boolean", nullable=true)
     */
    private $marcada;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="date", nullable=false)
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_modificacion", type="date", nullable=true)
     */
    private $fechaModificacion;

    /**
     * @ORM\OneToMany(targetEntity="Incidencia", mappedBy="comunidadId")
     */
    private $incidencias;

    /**
     * @ORM\OneToMany(targetEntity="Persona", mappedBy="comunidadId")
     */
    private $personas;

    public function __construct() {
        $this->incidencias = new ArrayCollection();
        $this->personas = new ArrayCollection();
    }

    
    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getPresidente() {
        return $this->presidente;
    }

    function getCif() {
        return $this->cif;
    }

    function getCuenta() {
        return $this->cuenta;
    }

    function getMarcada() {
        return $this->marcada;
    }

    function getFechaCreacion() {
        return $this->fechaCreacion;
    }

    function getFechaModificacion() {
        return $this->fechaModificacion;
    }

    function getIncidencias() {
        return $this->incidencias;
    }

    function getPersonas() {
        return $this->personas;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setPresidente($presidente) {
        $this->presidente = $presidente;
    }

    function setCif($cif) {
        $this->cif = $cif;
    }

    function setCuenta($cuenta) {
        $this->cuenta = $cuenta;
    }

    function setMarcada($marcada) {
        $this->marcada = $marcada;
    }

    function setFechaCreacion(\DateTime $fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;
    }

    function setFechaModificacion(\DateTime $fechaModificacion) {
        $this->fechaModificacion = $fechaModificacion;
    }

    function setIncidencias($incidencias) {
        $this->incidencias = $incidencias;
    }

    function setPersonas($personas) {
        $this->personas = $personas;
    }

    public function __toString() {
        return $this->nombre;
    }

    

    /**
     * Add incidencia
     *
     * @param \AppBundle\Entity\Incidencia $incidencia
     *
     * @return Comunidad
     */
    public function addIncidencia(\AppBundle\Entity\Incidencia $incidencia)
    {
        $this->incidencias[] = $incidencia;

        return $this;
    }

    /**
     * Remove incidencia
     *
     * @param \AppBundle\Entity\Incidencia $incidencia
     */
    public function removeIncidencia(\AppBundle\Entity\Incidencia $incidencia)
    {
        $this->incidencias->removeElement($incidencia);
    }

    /**
     * Add persona
     *
     * @param \AppBundle\Entity\Persona $persona
     *
     * @return Comunidad
     */
    public function addPersona(\AppBundle\Entity\Persona $persona)
    {
        $this->personas[] = $persona;

        return $this;
    }

    /**
     * Remove persona
     *
     * @param \AppBundle\Entity\Persona $persona
     */
    public function removePersona(\AppBundle\Entity\Persona $persona)
    {
        $this->personas->removeElement($persona);
    }
}
