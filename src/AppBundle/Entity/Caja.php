<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Caja
 *
 * @ORM\Table(name="caja", indexes={@ORM\Index(name="caja_FI_1", columns={"comunidad_id"})})
 * @ORM\Entity
 */
class Caja
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="notas", type="text", length=65535, nullable=true)
     */
    private $notas;

    /**
     * @var string
     *
     * @ORM\Column(name="texto", type="string", length=128, nullable=false)
     */
    private $texto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="date", nullable=true)
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_modificacion", type="date", nullable=true)
     */
    private $fechaModificacion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="marcada", type="boolean", nullable=true)
     */
    private $marcada;

    /**
     * @var integer
     *
     * @ORM\Column(name="comunidad_id", type="integer", nullable=true)
     */
    private $comunidadId;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set notas
     *
     * @param string $notas
     *
     * @return Caja
     */
    public function setNotas($notas)
    {
        $this->notas = $notas;

        return $this;
    }

    /**
     * Get notas
     *
     * @return string
     */
    public function getNotas()
    {
        return $this->notas;
    }

    /**
     * Set texto
     *
     * @param string $texto
     *
     * @return Caja
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return Caja
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaModificacion
     *
     * @param \DateTime $fechaModificacion
     *
     * @return Caja
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * Set marcada
     *
     * @param boolean $marcada
     *
     * @return Caja
     */
    public function setMarcada($marcada)
    {
        $this->marcada = $marcada;

        return $this;
    }

    /**
     * Get marcada
     *
     * @return boolean
     */
    public function getMarcada()
    {
        return $this->marcada;
    }

    /**
     * Set comunidadId
     *
     * @param integer $comunidadId
     *
     * @return Caja
     */
    public function setComunidadId($comunidadId)
    {
        $this->comunidadId = $comunidadId;

        return $this;
    }

    /**
     * Get comunidadId
     *
     * @return integer
     */
    public function getComunidadId()
    {
        return $this->comunidadId;
    }
}
