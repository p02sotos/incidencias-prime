<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MovimientoCaja
 *
 * @ORM\Table(name="movimiento_caja", indexes={@ORM\Index(name="movimiento_caja_FI_1", columns={"caja_id"})})
 * @ORM\Entity
 */
class MovimientoCaja
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_movimiento", type="date", nullable=false)
     */
    private $fechaMovimiento;

    /**
     * @var string
     *
     * @ORM\Column(name="concepto", type="string", length=1024, nullable=false)
     */
    private $concepto;

    /**
     * @var boolean
     *
     * @ORM\Column(name="devolver", type="boolean", nullable=true)
     */
    private $devolver;

    /**
     * @var float
     *
     * @ORM\Column(name="cantidad", type="float", precision=10, scale=0, nullable=false)
     */
    private $cantidad;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="date", nullable=true)
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_modificacion", type="date", nullable=true)
     */
    private $fechaModificacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="caja_id", type="integer", nullable=true)
     */
    private $cajaId;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fechaMovimiento
     *
     * @param \DateTime $fechaMovimiento
     *
     * @return MovimientoCaja
     */
    public function setFechaMovimiento($fechaMovimiento)
    {
        $this->fechaMovimiento = $fechaMovimiento;

        return $this;
    }

    /**
     * Get fechaMovimiento
     *
     * @return \DateTime
     */
    public function getFechaMovimiento()
    {
        return $this->fechaMovimiento;
    }

    /**
     * Set concepto
     *
     * @param string $concepto
     *
     * @return MovimientoCaja
     */
    public function setConcepto($concepto)
    {
        $this->concepto = $concepto;

        return $this;
    }

    /**
     * Get concepto
     *
     * @return string
     */
    public function getConcepto()
    {
        return $this->concepto;
    }

    /**
     * Set devolver
     *
     * @param boolean $devolver
     *
     * @return MovimientoCaja
     */
    public function setDevolver($devolver)
    {
        $this->devolver = $devolver;

        return $this;
    }

    /**
     * Get devolver
     *
     * @return boolean
     */
    public function getDevolver()
    {
        return $this->devolver;
    }

    /**
     * Set cantidad
     *
     * @param float $cantidad
     *
     * @return MovimientoCaja
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return float
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return MovimientoCaja
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion()
    {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaModificacion
     *
     * @param \DateTime $fechaModificacion
     *
     * @return MovimientoCaja
     */
    public function setFechaModificacion($fechaModificacion)
    {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion()
    {
        return $this->fechaModificacion;
    }

    /**
     * Set cajaId
     *
     * @param integer $cajaId
     *
     * @return MovimientoCaja
     */
    public function setCajaId($cajaId)
    {
        $this->cajaId = $cajaId;

        return $this;
    }

    /**
     * Get cajaId
     *
     * @return integer
     */
    public function getCajaId()
    {
        return $this->cajaId;
    }
}
