<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Aviso
 *
 * @ORM\Table(name="aviso", indexes={@ORM\Index(name="aviso_FI_1", columns={"incidencia_id"}), @ORM\Index(name="aviso_FI_2", columns={"tarea_id"}), @ORM\Index(name="aviso_FI_3", columns={"comunidad_id"}), @ORM\Index(name="aviso_FI_4", columns={"persona_id"}), @ORM\Index(name="aviso_FI_5", columns={"entrega_id"}), @ORM\Index(name="aviso_FI_6", columns={"contabilidad_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AvisoRepository")
 */
class Aviso {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=1024, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", length=65535, nullable=true)
     */
    private $descripcion;

    /**
     * @var integer
     *
     * @ORM\Column(name="prioridad", type="integer", nullable=false)
     */
    private $prioridad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="marcada", type="boolean", nullable=true)
     */
    private $marcada;

    /**
     * @var boolean
     *
     * @ORM\Column(name="eliminado", type="boolean", nullable=true)
     */
    private $eliminado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_aviso", type="date", nullable=true)
     */
    private $fechaAviso;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="date", nullable=true)
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_modificacion", type="date", nullable=true)
     */
    private $fechaModificacion;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="Incidencia", inversedBy="avisos")
     * @ORM\JoinColumn(name="incidencia_id", referencedColumnName="id")
     *  
     * 
     */
    private $incidenciaId;

    /**
     * @var integer
     *
     * @ORM\Column(name="comunidad_id", type="integer", nullable=true)
     */
    private $comunidadId;

    /**
     * @var integer
     *
     * @ORM\Column(name="persona_id", type="integer", nullable=true)
     */
    private $personaId;

    /**
     * @var integer
     *
     * @ORM\Column(name="entrega_id", type="integer", nullable=true)
     */
    private $entregaId;

    /**
     * @var integer
     *
     * @ORM\Column(name="contabilidad_id", type="integer", nullable=true)
     */
    private $contabilidadId;

    /**
     * @var integer
     *
     * @ORM\Column(name="tarea_id", type="integer", nullable=true)
     */
    private $tareaId;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Aviso
     */
    public function setNombre($nombre) {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre() {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Aviso
     */
    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion() {
        return $this->descripcion;
    }

    /**
     * Set prioridad
     *
     * @param integer $prioridad
     *
     * @return Aviso
     */
    public function setPrioridad($prioridad) {
        $this->prioridad = $prioridad;

        return $this;
    }

    /**
     * Get prioridad
     *
     * @return integer
     */
    public function getPrioridad() {
        return $this->prioridad;
    }

    /**
     * Set marcada
     *
     * @param boolean $marcada
     *
     * @return Aviso
     */
    public function setMarcada($marcada) {
        $this->marcada = $marcada;

        return $this;
    }

    /**
     * Get marcada
     *
     * @return boolean
     */
    public function getMarcada() {
        return $this->marcada;
    }

    /**
     * Set eliminado
     *
     * @param boolean $eliminado
     *
     * @return Aviso
     */
    public function setEliminado($eliminado) {
        $this->eliminado = $eliminado;

        return $this;
    }

    /**
     * Get eliminado
     *
     * @return boolean
     */
    public function getEliminado() {
        return $this->eliminado;
    }

    /**
     * Set fechaAviso
     *
     * @param \DateTime $fechaAviso
     *
     * @return Aviso
     */
    public function setFechaAviso($fechaAviso) {
        $this->fechaAviso = $fechaAviso;

        return $this;
    }

    /**
     * Get fechaAviso
     *
     * @return \DateTime
     */
    public function getFechaAviso() {
        return $this->fechaAviso;
    }

    /**
     * Set fechaCreacion
     *
     * @param \DateTime $fechaCreacion
     *
     * @return Aviso
     */
    public function setFechaCreacion($fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;

        return $this;
    }

    /**
     * Get fechaCreacion
     *
     * @return \DateTime
     */
    public function getFechaCreacion() {
        return $this->fechaCreacion;
    }

    /**
     * Set fechaModificacion
     *
     * @param \DateTime $fechaModificacion
     *
     * @return Aviso
     */
    public function setFechaModificacion($fechaModificacion) {
        $this->fechaModificacion = $fechaModificacion;

        return $this;
    }

    /**
     * Get fechaModificacion
     *
     * @return \DateTime
     */
    public function getFechaModificacion() {
        return $this->fechaModificacion;
    }

    /**
     * Set incidenciaId
     *
     * @param integer $incidenciaId
     *
     * @return Aviso
     */
    public function setIncidenciaId($incidenciaId) {
        $this->incidenciaId = $incidenciaId;

        return $this;
    }

    /**
     * Get incidenciaId
     *
     * @return integer
     */
    public function getIncidenciaId() {
        return $this->incidenciaId;
    }

    /**
     * Set comunidadId
     *
     * @param integer $comunidadId
     *
     * @return Aviso
     */
    public function setComunidadId($comunidadId) {
        $this->comunidadId = $comunidadId;

        return $this;
    }

    /**
     * Get comunidadId
     *
     * @return integer
     */
    public function getComunidadId() {
        return $this->comunidadId;
    }

    /**
     * Set personaId
     *
     * @param integer $personaId
     *
     * @return Aviso
     */
    public function setPersonaId($personaId) {
        $this->personaId = $personaId;

        return $this;
    }

    /**
     * Get personaId
     *
     * @return integer
     */
    public function getPersonaId() {
        return $this->personaId;
    }

    /**
     * Set entregaId
     *
     * @param integer $entregaId
     *
     * @return Aviso
     */
    public function setEntregaId($entregaId) {
        $this->entregaId = $entregaId;

        return $this;
    }

    /**
     * Get entregaId
     *
     * @return integer
     */
    public function getEntregaId() {
        return $this->entregaId;
    }

    /**
     * Set contabilidadId
     *
     * @param integer $contabilidadId
     *
     * @return Aviso
     */
    public function setContabilidadId($contabilidadId) {
        $this->contabilidadId = $contabilidadId;

        return $this;
    }

    /**
     * Get contabilidadId
     *
     * @return integer
     */
    public function getContabilidadId() {
        return $this->contabilidadId;
    }

    /**
     * Set tareaId
     *
     * @param integer $tareaId
     *
     * @return Aviso
     */
    public function setTareaId($tareaId) {
        $this->tareaId = $tareaId;

        return $this;
    }

    /**
     * Get tareaId
     *
     * @return integer
     */
    public function getTareaId() {
        return $this->tareaId;
    }
    
    

}
