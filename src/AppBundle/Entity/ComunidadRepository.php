<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Entity;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Description of ComunidadRepository
 *
 * @author Ser
 */
class ComunidadRepository extends EntityRepository {
    
     public function findAllComunidades() {
        $query = $this->getEntityManager()
                ->createQuery(
                'SELECT c FROM AppBundle:Comunidad c            
            '
        );
        try {
            return $query->getResult(Query::HYDRATE_SCALAR);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
     public function findAllComunidadesMin() {
        $query = $this->getEntityManager()
                ->createQuery(
                'SELECT c.id, c.nombre FROM AppBundle:Comunidad c            
            '
        );
        try {
            return $query->getResult(Query::HYDRATE_SCALAR);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}
