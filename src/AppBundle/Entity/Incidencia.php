<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;

/**
 * Incidencia
 * 
 * @ORM\Table(name="incidencia", options={"collate"="utf8_unicode_ci", "charset"="utf8"}, indexes={@ORM\Index(name="incidencia_FI_1", columns={"comunidad_id"}), @ORM\Index(name="incidencia_FI_2", columns={"persona_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\IncidenciaRepository")
 * @ExclusionPolicy("all")
 */
class Incidencia {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose
     * @Groups({"list"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="origen", type="string", length=1024, nullable=true)
     * @Expose
     * @Groups({"list"})
     * 
     */
    private $origen;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_persona", type="string", length=1024, nullable=true)
     * 
     * @Groups({"list"})
     */
    private $nombrePersona;

    /**
     * @var string
     *
     * @ORM\Column(name="breve", type="string", length=1024, nullable=false)
     * @Expose
     * @Groups({"me"})
     */
    private $breve;

    /**
     * @var string
     *
     * @ORM\Column(name="expediente", type="string", length=256, nullable=true)
     * @Expose
     * @Groups({"list"})
     */
    private $expediente;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", length=65535, nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="atencion", type="string", length=256, nullable=true)
     *  @Expose
     * @Groups({"list"})
     */
    private $atencion;

    /**
     * @var integer
     *
     * @ORM\Column(name="prioridad", type="integer", nullable=true)
     *  @Expose
     * @Groups({"list"})
     */
    private $prioridad;

    /**
     * @var boolean
     *
     * @ORM\Column(name="resuelta", type="boolean", nullable=true)
     *  @Expose
     * @Groups({"list"})
     */
    private $resuelta;

    /**
     * @var boolean
     *
     * @ORM\Column(name="marcada", type="boolean", nullable=true)
     *  @Expose
     * @Groups({"list"})
     */
    private $marcada;

    /**
     * @var boolean
     *
     * @ORM\Column(name="eliminado", type="boolean", nullable=true)
     *  @Expose
     * @Groups({"list"})
     */
    private $eliminado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_creacion", type="datetime", nullable=false)
     * *  @Expose
     * @Groups({"list"})
     */
    private $fechaCreacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_modificacion", type="datetime", nullable=true)
     *  @Expose
     * @Groups({"list"})
     */
    private $fechaModificacion;

    /**
     * @var \Date
     *
     * @ORM\Column(name="fecha_incidencia", type="date", nullable=true)
     *  @Expose
     * @Groups({"list"})
     */
    private $fechaIncidencia;

    /**
     * @var \Date
     *
     * @ORM\Column(name="fecha_resolucion", type="date", nullable=false)
     *  @Expose
     * @Groups({"list"})
     */
    private $fechaResolucion;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="Comunidad", inversedBy="incidencias")
     * @ORM\JoinColumn(name="comunidad_id", referencedColumnName="id")
     *  
     * 
     */
    private $comunidadId;

    /**
     * 
     * @ORM\OneToMany(targetEntity="Seguimiento", mappedBy="incidenciaId")
     * 
     */
    private $seguimientos;

    /**
     * 
     * @ORM\OneToMany(targetEntity="Telefono", mappedBy="incidenciaId",  cascade={"persist", "remove"})
     * 
     */
    private $telefonos;

    /**
     * 
     * @ORM\OneToMany(targetEntity="Aviso", mappedBy="incidenciaId",  cascade={"persist", "remove"})
     * 
     */
    private $avisos;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="Persona", inversedBy="incidencias")
     * @ORM\JoinColumn(name="persona_id", referencedColumnName="id")
     * 
     */
    private $personaId;

    public function __construct() {
        $this->seguimientos = new ArrayCollection();
        $this->telefonos = new ArrayCollection();
        $this->avisos = new ArrayCollection();
    }

    function getId() {
        return $this->id;
    }

    function getOrigen() {
        return $this->origen;
    }

    function getNombrePersona() {
        return $this->nombrePersona;
    }

    function getBreve() {
        return $this->breve;
    }

    function getExpediente() {
        return $this->expediente;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function getAtencion() {
        return $this->atencion;
    }

    function getPrioridad() {
        return $this->prioridad;
    }

    function getResuelta() {
        return $this->resuelta;
    }

    function getMarcada() {
        return $this->marcada;
    }

    function getEliminado() {
        return $this->eliminado;
    }

    function getFechaCreacion() {
        return $this->fechaCreacion;
    }

    function getFechaModificacion() {
        return $this->fechaModificacion;
    }

    function getFechaIncidencia() {
        return $this->fechaIncidencia;
    }

    function getFechaResolucion() {
        return $this->fechaResolucion;
    }

    function getComunidadId() {
        return $this->comunidadId;
    }

    function getPersonaId() {
        return $this->personaId;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setOrigen($origen) {
        $this->origen = $origen;
    }

    function setNombrePersona($nombrePersona) {
        $this->nombrePersona = $nombrePersona;
    }

    function setBreve($breve) {
        $this->breve = $breve;
    }

    function setExpediente($expediente) {
        $this->expediente = $expediente;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    function setAtencion($atencion) {
        $this->atencion = $atencion;
    }

    function setPrioridad($prioridad) {
        $this->prioridad = $prioridad;
    }

    function setResuelta($resuelta) {
        $this->resuelta = $resuelta;
    }

    function setMarcada($marcada) {
        $this->marcada = $marcada;
    }

    function setEliminado($eliminado) {
        $this->eliminado = $eliminado;
    }

    function setFechaCreacion(\DateTime $fechaCreacion) {
        $this->fechaCreacion = $fechaCreacion;
    }

    function setFechaModificacion(\DateTime $fechaModificacion) {
        $this->fechaModificacion = $fechaModificacion;
    }

    function setFechaIncidencia(\DateTime $fechaIncidencia) {
        $this->fechaIncidencia = $fechaIncidencia;
    }

    function setFechaResolucion(\DateTime $fechaResolucion) {
        $this->fechaResolucion = $fechaResolucion;
    }

    function setComunidadId($comunidadId) {
        $this->comunidadId = $comunidadId;
    }

    function setPersonaId($personaId) {
        $this->personaId = $personaId;
    }

    function getSeguimientos() {
        return $this->seguimientos;
    }

    function setSeguimientos($seguimientos) {
        $this->seguimientos = $seguimientos;
    }

    /**
     * Add seguimiento
     *
     * @param \AppBundle\Entity\Seguimiento $seguimiento
     *
     * @return Incidencia
     */
    public function addSeguimiento(\AppBundle\Entity\Seguimiento $seguimiento) {
        $seguimiento->setIncidenciaId($this);
        $this->seguimientos->add($seguimiento);

        return $this;
    }

    /**
     * Remove seguimiento
     *
     * @param \AppBundle\Entity\Seguimiento $seguimiento
     */
    public function removeSeguimiento(\AppBundle\Entity\Seguimiento $seguimiento) {
        $this->seguimientos->removeElement($seguimiento->getId());
    }

    /**
     * Add telefonos
     *
     * @param \AppBundle\Entity\Seguimiento $telefono
     *
     * @return Incidencia
     */
    public function addTelefonos(\AppBundle\Entity\Telefono $telefono) {
        $telefono->setIncidenciaId($this);
        $this->telefonos->add($telefono);

        return $this;
    }

    /**
     * Remove $telefono
     *
     * @param \AppBundle\Entity\Seguimiento $telefono
     */
    public function removeTelefonos(\AppBundle\Entity\Telefono $telefono) {
        $this->$telefonos->removeElement($telefono);
    }

    /**
     * Add telefonos
     *
     * @param \AppBundle\Entity\Seguimiento $aviso
     *
     * @return Incidencia
     */
    public function addAvisos(\AppBundle\Entity\Aviso $aviso) {
        $aviso->setIncidenciaId($this);
        $this->avisos->add($aviso);

        return $this;
    }

    /**
     * Remove $telefono
     *
     * @param \AppBundle\Entity\Seguimiento $telefono
     */
    public function removeAvisos(\AppBundle\Entity\Telefono $aviso) {
        $this->avisos->removeElement($aviso);
    }

    function getAvisos() {
        return $this->avisos;
    }

    function setAvisos($avisos) {
        $this->avisos = $avisos;
    }

    function getTelefonos() {
        return $this->telefonos;
    }

    function setTelefonos($telefonos) {
        $this->telefonos = $telefonos;
    }

    public function __toString() {
        try {
            return (string)$this->id;
        } catch (Exception $exception) {
            return 'null';
        }
    }

}
