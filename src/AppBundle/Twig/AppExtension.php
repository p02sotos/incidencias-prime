<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ObjectToArrayFilter
 *
 * @author Ser
 */

namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension {

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('to_array', array($this, 'to_arrayFilter')),
            new \Twig_SimpleFilter('truncate', array($this, 'truncate')),
        );
    }

    public function to_arrayFilter($object) {

        return (array) $object;
    }
    
     public function truncate($string, $size) {

        return substr($string, 0, $size);
    }

    public function getName() {
        return 'app_extension';
    }

}
